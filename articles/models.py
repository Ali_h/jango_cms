from __future__ import unicode_literals
from django.db import models

class article(models.Model):
    title = models.CharField(max_length = 15)
    keyword = models.CharField(max_length = 15)
    text = models.CharField(max_length = 100)
    score = models.IntegerField(blank=True, null=True)
    class Meta:
        ordering = ["score"]
    def __str__(self):
        return "title : {} text : {} score : {}" .format(self.title , self.text , self.score)

class comment(models.Model):
    text = models.CharField(max_length = 100)
    article_id = models.ForeignKey(article , on_delete=models.CASCADE)
    class Meta:
        ordering=['article_id']
    def __str__(self):
        return 'text : {}' .format(self.text)
