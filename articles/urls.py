from django.conf.urls import url
from django.contrib import admin
from articles.views import *

app_name = 'ali'
urlpatterns = [
    url(r'^add_article/', add_article , name = "add_article"),
]