from django.shortcuts import render , render_to_response , get_object_or_404 ,get_list_or_404
from django.urls import reverse
from django.http import HttpResponse , Http404 , HttpResponseRedirect , JsonResponse
from django.views.decorators.csrf import csrf_exempt
from users.models import *
from articles.models import *
from validate_email import validate_email
import math

import MySQLdb
db = MySQLdb.connect(host="127.0.0.1" , user="root" , passwd="3138" , db='cms')
cur=db.cursor()


@csrf_exempt
def add_article(request):
    if request.method == "POST" :
        if request.session["login"]: 
            title = request.POST["title"]
            text = request.POST["text"]
            keyword = request.POST["keyword"]
            exist_article = article.objects.filter(title=title , keyword=keyword , text=text)
            if len(title) == 0 or len(text) == 0 or len(keyword) == 0 :
                return JsonResponse({"error_len" : 1})
            elif exist_article :
                return JsonResponse({"exist_article" : 1})
            else :
                create_article = article.objects.create(title=title , keyword=keyword , text=text)
                return JsonResponse({"add_success" : 1 , "title" : title , "text" : text , "keyword" : keyword})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})

@csrf_exempt
def delete_article(request):
    if request.method == "POST" :
        if request.session["login"]:
            article_id = request.POST["article_id"]
            articles = article.objects.filter(id=article_id)
            if not article :
                return JsonResponse({"error_exist_article" : 1})
            else : 
                delete_article = articles.delete()
                return JsonResponse({"delete" : 1})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})

@csrf_exempt
def search_article(request):
    if request.method == "POST" :
        if request.session["login"]:
            search_keyword = request.POST["search_keyword"]
            counter = int(request.POST["counter"])
            if len(search_keyword) == 0 :
                return JsonResponse({"error_len" : 1})
            else:
                limit = counter * 10
                cur.execute("SELECT * FROM articles_article WHERE keyword = %s LIMIT 10 offset "+str(limit) ,(search_keyword,))
                articles = cur.fetchall()
                db.commit()
                if(articles) :
                    count_article = article.objects.filter(keyword=search_keyword).count()
                    button_count = int(math.ceil(float(count_article) / 10))
                    return JsonResponse({"success" : 1 , "article" : articles , "button_count" : button_count , "keyword" : search_keyword})
                else:
                    return JsonResponse({"error_exits_article" : 1})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})

@csrf_exempt
def add_score(request):
    if request.method == "POST" :
        if request.session["login"]: 
            score = request.POST["score"]
            if int(score) > 5 :
                return JsonResponse({"error_len_score" : 1})
            else :
                article_id = request.POST["id"]
                add_score = article.objects.filter(id=article_id).update(score=score)
                return JsonResponse({"add_score" : 1})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})
    
@csrf_exempt
def edit_article(request):
    if request.method == "POST" :
        if request.session["login"]:
            title = request.POST["title"]
            text = request.POST["text"]
            keyword = request.POST["keyword"]
            article_id = request.POST["id"]
            exist_article = article.objects.filter(title=title , keyword=keyword , text=text)
            if len(title) == 0 or len(text) == 0 or len(keyword) == 0:
                return JsonResponse({"error_len" :1})
            elif exist_article :
                return JsonResponse({"exist_article" : 1})
            else :
                edit_article = article.objects.filter(id=article_id).update(title=title , keyword=keyword , text=text , score=None)
                return JsonResponse({"success_edit" : 1})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})
                

@csrf_exempt
def comment_article(request):
    if request.method == "POST" :
        if request.session["login"]:
            comment = request.POST["comment"]
            article_id = request.POST["id"]
            if len(comment) == 0 :
                return JsonResponse({"error_len" :1})
            else:
                articles = article.objects.filter(id=article_id)
                comment(text = comment , article_id = articles)
                comment.save()
                return JsonResponse({"success" : 1})
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})
