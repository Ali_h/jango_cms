from django.conf.urls import url
from django.contrib import admin
from users.views import *

app_name = 'users'
urlpatterns = [
    url(r'^sign_in/', sign_in_sing_up , name = "sign_in"),
    url(r'^sign_out/', sign_out , name = "sign_out"),
]