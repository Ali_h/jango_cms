from __future__ import unicode_literals
from django.db import models

class users (models.Model):
    user_name = models.CharField (max_length = 20)
    password = models.CharField (max_length = 25)
    email = models.EmailField (blank=True, null=True)
    class Meta:
        ordering = ["user_name"]
    def __str__(self):
        return 'user_name : {} password : {}' .format(self.user_name , self.password)