from django.shortcuts import render , render_to_response , get_object_or_404 ,get_list_or_404
from django.urls import reverse
from django.http import HttpResponse , Http404 , HttpResponseRedirect , JsonResponse
from django.views.decorators.csrf import csrf_exempt
from users.models import *
from articles.models import *
from validate_email import validate_email
import math

import MySQLdb
db = MySQLdb.connect(host="127.0.0.1" , user="root" , passwd="3138" , db='cms')
cur=db.cursor()


def athenticate(username,password):	#This function checks the correction of username and password
	User = users.objects.filter(user_name=username , password=password)
	if len(User) == 1:
		return username
	elif len(User) == 0:
		return False

# MOSHKEL DAR SESSION HA DARIM

@csrf_exempt
def sign_in_sing_up(request):
    if request.method == "POST" :
        if request.POST["action"] == "sign_in":
            username = request.POST["username"]
            password = request.POST["password"]
            if len(username) == 0 and len(password) == 0 :
                return JsonResponse({"user_pass_error" : 1})
            elif len(username) == 0:
                return JsonResponse({"username_error" : 1})
            elif len(password) == 0:
                return JsonResponse({"pass_error" : 1})
            else :
                check_user = athenticate(username,password)
                if check_user == False:
                    return JsonResponse({"not_user" : 1})
                else:
                    request.session["login"] = True
                    request.session["username"] = username
                    return JsonResponse({"login" : 1})
                
        elif request.POST["action"] == "sign_up":
            username = request.POST["username"]
            password = request.POST["password"]
            email = request.POST["email"]
            if len(username) == 0 or len(password) == 0:
                return JsonResponse({"error_len" : 1})
            elif len(password) < 6 :
                return JsonResponse({"pass_error":1})
            elif 'email' in request.POST and len(request.POST["email"]) != 0 and validate_email(request.POST["email"]) == False:
                return JsonResponse({"error_email" : 1})
            else:
                check_username = users.objects.filter(user_name=username)
                if check_username :
                    return JsonResponse({"exist_user" : 1})
                else :
                    create_user = users.objects.create(user_name=username , password=password , email=email)
                    return JsonResponse({"register" : 1})
    else : 
        if request.session["login"]: 
            return HttpResponseRedirect('/home')
        else :
            return render_to_response("login-register.html")

def sign_out(request):
    try :
        request.session["login"] = False
        del request.session["username"]
        return HttpResponseRedirect(reverse('users:sign_in'))
    except KeyError :
        print("WAENING !!")

@csrf_exempt
def home(request):
    if request.method == "GET" :
        if request.session["login"]:
            show_article = article.objects.order_by("id")[:10]
            count_article = article.objects.count()
            button_count = int(math.ceil(float(count_article) / 10))
            return render_to_response("home.html" , {"articles" : show_article , "button_count" : button_count})
        else:
            return HttpResponseRedirect(reverse('users:sign_in'))

    if request.method == "POST" :
        if request.session["login"]:
            if 'action' in request.POST and request.POST["action"] == "show_home":
                return JsonResponse({"show_home" : 1})
            else:
                counter = int(request.POST["counter"])
                limit = counter * 10
                cur.execute("SELECT * FROM articles_article limit 10 offset "+str(limit))
                articles=cur.fetchall()
                db.commit()
                return JsonResponse({"article" : articles})
            
        else :
            request.session["login"] = False
            return JsonResponse({"error_login" : 1})
