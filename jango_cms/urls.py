from django.conf.urls import url , include
from django.contrib import admin
from users.views import *
from articles.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^user/', include('users.urls')),
    url(r'^$', sign_in_sing_up),
    url(r'^home/$', home),
    url(r'^add_article/', add_article),
    url(r'^search_article/', search_article),
    url(r'^add_score/$', add_score),
    url(r'^delete_article/$', delete_article),
    url(r'^edit_article/$', edit_article),
    url(r'^comment_article/$', comment_article),
]
